package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.ListIterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        if (x.size() > y.size())
            return false;

        if (x.isEmpty())
            return true;

        ListIterator xiter = x.listIterator();
        ListIterator yiter = y.listIterator();

        while (yiter.hasNext()) {
            if (!xiter.next().equals(yiter.next()))
                xiter.previous();

            else if (!xiter.hasNext())
                return true;
        }

        return false;
    }
}
