package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int rows = calculateSize(inputNumbers.size());
        int cols = 2 * rows - 1;

        if (!isValid(rows, inputNumbers.size()) || inputNumbers.contains(null))
            throw new CannotBuildPyramidException("Invalid sample");

        int[][] res = new int[rows][cols];
        Collections.sort(inputNumbers);
        Queue<Integer> queue = new LinkedList<Integer>(inputNumbers);

        int step = 1;
        int startCol = (cols) / 2;
        int col = startCol;

        for (int row = 0; row < rows; ++row) {
            for (; col - startCol <= step; col += 2) {
                try {
                    res[row][col] = queue.remove();
                } catch (NoSuchElementException nee) {
                    nee.printStackTrace();
                }
            }
            ++step;
            col = (cols) / 2 - row - 1;
        }
        return res;
    }

    private int calculateSize(int sampleSize) {
        if (sampleSize <= 0)
            throw new CannotBuildPyramidException("Invalid sample size");
        return (int) Math.sqrt(8 * sampleSize + 1) / 2;
    }

    private boolean isValid(int rowsCount, int sampleSize) {
        return (rowsCount + 1) * rowsCount / 2 == sampleSize;
    }
}
